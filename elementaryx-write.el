(use-package elementaryx-base)

(defcustom elementaryx-org-cite-global-bibliography '("~/references/references.bib")
  "List of bibliography files for org-cite."
  :type '(repeat file)
  :group 'elementaryx)

(defcustom elementaryx-biblio-download-directory "~/references/files/"
  "Where to put downloaded papers. Used by biblio (biblio-download-directory) and citar (citar-libary-paths)."
  :type 'directory
  :group 'elementaryx)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Citar citation handling ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; We use citar for:
;; - inserting existing (bibliographic) references (from org-cite-global-bibliography) into org or latex files (C-c i c)
;; - opening notes (with citar-open-notes) or associated ressources (with citar-open) with those references (in elementaryx-biblio-download-directory)
(use-package citar
  ;; :no-require
  :init
  (defun __elementaryx-filter-existing-files (file-list)
  "Return a list of files from FILE-LIST that actually exist.
Warns if a file does not exist."
  (delq nil
        (mapcar (lambda (file)
                  (if (file-exists-p file)
                      file
                    (message "Warning: file %s does not exist." file)
                    nil))
                file-list)))
  :config
  (which-key-add-key-based-replacements "C-c i c" "citation")
  (setq org-cite-global-bibliography (__elementaryx-filter-existing-files elementaryx-org-cite-global-bibliography))
  :custom
  ;; Insertion
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar)
  (citar-bibliography org-cite-global-bibliography)
  ;; Associated documents (pdf, ...)
  (citar-library-paths (list elementaryx-biblio-download-directory))
  ;; (citar-notes-paths '("~/references/notes")) ;; OFF: We let org-roam handle it for now
  :hook
  (LaTeX-mode . citar-capf-setup)
  (org-mode . citar-capf-setup)
  :bind (("C-c n c N" . citar-open-notes) ;; Handled with org-roam when used together with citar-org-roam (in elementaryx-org)
	 ("C-c n c o" . citar-open) ;; Handled with org-roam when used together with citar-org-roam (in elementaryx-org)
	 ("C-c n c n" . citar-open-note)
	 ("C-c n c c" . citar-create-note)
	 ("C-c n c O" . citar-open-entry)
	 ("C-c n c A" . citar-add-file-to-library)
	 ("C-c i c k" . citar-insert-keys)
	 ("C-c i c C" . citar-insert-reference)
	 ))

(use-package citar
  :after org
  :bind (:map org-mode-map
	      ("C-c i c c" . org-cite-insert)
	      ("C-c [" . org-cite-insert)
	      ("C-c i c e" . citar-insert-edit)
	      ("C-c E" . citar-export-local-bib-file)
	      ))

(use-package citar
  :after latex
  :bind (:map LaTeX-mode-map
	      ("C-c i c c" . citar-insert-citation)
	      ("C-c [" . citar-insert-citation)
	      ("C-c i c e" . citar-insert-edit)
	      ("C-c E" . citar-export-local-bib-file)
	      ))
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/TeX-Mode.html

(use-package citar-embark
  :after citar embark
  ;; :no-require
  :config (citar-embark-mode))

;; We use bibtex (natively shipped with emacs) for:
;; - formatting (C-c C-c) and aligning entries (C-c C-q, but also automatic with C-c C-c thanks to (bibtex-entry-format t));
;; - inserting entries with templae (C-c C-b);
;; - reformat all entries in region or buffer (bibtex-reformat);
;; - sort all entries in tn the buffer by their keys (bibtex-sort-buffer).
;; Menu-bar: Bibtex-Edit.
;; Key bindings (emacs default):
;; - C-c C-c bibtex-clean-entry: Format the entry;
;; - C-u C-C C-C bibtex-clean-entry with prefix argument: Format the entry and generate key;
;; - C-c C-q bibtex-fill-entry: Aign the fields;
;; - C-c C-b bibtex-entry: Insert a template for a bibtex entry.
(use-package bibtex
  :bind (("C-c t b o" . elementaryx-toggle-global-bibtex-options) ;; We want to make sure not confuse with "C-c t B" (build-menu) so we add an extra 'o' confirmation key"
	 :map bibtex-mode-map
	 ("C-c i b" . citar-insert-bibtex) ;; TODO refine whether to set it in the commands section e.g.
	 ("C-c [" . citar-insert-bibtex)   ;; TODO refine whether to set it in the commands section e.g.
	 ("C-c ^" . bibtex-sort-buffer) ;; similar to org-mode sort https://orgmode.org/worg/orgcard.html
	 ;; Note that the nice bibtex-url is already bound to "C-c C-l"
	 ("C-c i c O" . elementaryx-setup-bibtex-options-as-file-local-variables)
	 ("C-c i c o" . elementaryx-setup-bibtex-options-in-buffer))
  :init
  (which-key-add-key-based-replacements "C-c t B" "bibtex-global-options")
  :config

  ;; Bibtex options

  ;; C-c C-c   for cleanup and generating the key
  ;; C-c C-q   for formatting the enty

  ;; The default choice of bibtex options themselves is inspired from
  ;; input of Alfredo Buttari and from
  ;; http://www.jonathanleroux.org/bibtex-mode.html

  ;; We first define these ElementaryX bibtex options
  (defcustom elementaryx-bibtex-options
    '((bibtex-maintain-sorted-entries . t)
      (bibtex-autokey-edit-before-use . t)
      (bibtex-autokey-additional-names . "ea")
      (bibtex-autokey-name-length . 1)
      (bibtex-autokey-name-separator . ".")
      (bibtex-autokey-name-year-separator . ":")
      (bibtex-autokey-names . 4)
      (bibtex-autokey-titleword-length . 0)
      (bibtex-autokey-year-title-separator . "")
      (bibtex-entry-format . t)
      (bibtex-text-indentation . 18)
      (bibtex-align-at-equal-sign . t))
    "List of BibTeX variables and their values for file-local / buffer-local / global settings."
    :type '(repeat (cons (symbol :tag "Variable")
			 (sexp :tag "Value")))
    :group 'elementaryx)

  ;; Note that the keys of the entries can also be generated
  ;; automatically with the bibtool command:
  ;; bibtool -f "%-4.1n(author):%2d(year)" in.bib -o out.bib

  ;; This setup corresponds to the following local variables:

  ;; %%% Local Variables:
  ;; %%% bibtex-maintain-sorted-entries: t
  ;; %%% bibtex-autokey-edit-before-use: t
  ;; %%% bibtex-autokey-additional-names: "ea"
  ;; %%% bibtex-autokey-name-length: 1
  ;; %%% bibtex-autokey-name-separator: "."
  ;; %%% bibtex-autokey-name-year-separator: ":"
  ;; %%% bibtex-autokey-names: 4
  ;; %%% bibtex-autokey-titleword-length: 0
  ;; %%% bibtex-autokey-year-title-separator: ""
  ;; %%% bibtex-entry-format: t
  ;; %%% bibtex-text-indentation: 18
  ;; %%% bibtex-align-at-equal-sign: t
  ;; %%% End:

  ;; The following function does such a translation from
  ;; elementaryx-bibtex-options to file-local variables:
  (defun elementaryx-setup-bibtex-options-as-file-local-variables ()
  "Setup file-local variables for BibTeX interactively if in bibtex-mode.
If not in bibtex-mode, display a warning."
  (interactive)
  (if (derived-mode-p 'bibtex-mode)
      (dolist (var elementaryx-bibtex-options)
        (add-file-local-variable (car var) (cdr var)))
    (message "This function can only be run in BibTeX mode.")))

  ;; The following code aims at applying elementaryx-bibtex-options
  ;; in the current buffer

  ;; We first define a function to set local variables from a list of cons cells VARS-LIST
  (defun __elementaryx-set-local-variables-from-list (vars-list)
    "Set local variables from a list of cons cells VARS-LIST."
    (dolist (var vars-list)
      (let ((var-name (car var))
            (var-value (cdr var)))
	(set (make-local-variable var-name) var-value))))
  ;; Example usage
  ; (let ((vars '((toto . "toto") (tati . "tati"))))
  ;  (__elementaryx-set-local-variables-from-list vars))

  ;; We then use it to apply elementaryx-bibtex-options in the current buffer.
  (defun elementaryx-setup-bibtex-options-in-buffer ()
  "Set ElementaryX BibTeX options within the current buffer."
  (interactive)
  (if (derived-mode-p 'bibtex-mode)
      (progn
	(message "ElementaryX BibTeX options set within current buffer ...")
	(__elementaryx-set-local-variables-from-list elementaryx-bibtex-options)
        (message "ElementaryX BibTeX options set within current buffer."))
    (message "This function can only be run in BibTeX mode.")))

  (defvar __elementaryx-bibtex-options-global-enabled nil
    "Internal variable to track the state of global ElementaryX BibTeX settings.
Do not modify this variable directly.
`elementaryx-toggle-global-bibtex-options'.")

  ;; Global setup function that applies the same settings globally,
  ;; for all BibTeX files, when explicitly called.
  (defun elementaryx-toggle-global-bibtex-options ()
    "Toggle the global BibTeX settings."
    (interactive)
    (if __elementaryx-bibtex-options-global-enabled
	;; then: When global settings are enabled, disable them
	(progn
          (remove-hook 'bibtex-mode-hook 'elementaryx-setup-bibtex-options-in-buffer)
          (setq setq __elementaryx-bibtex-global-enabled nil)
          (message "Global ElementaryX BibTeX settings disabled."))
      ;; else: When global settings are disabled, enable them
      (add-hook 'bibtex-mode-hook 'elementaryx-setup-bibtex-options-in-buffer)
      (setq __elementaryx-bibtex-global-enabled t)
      (message "Global ElementaryX BibTeX settings enabled."))))

;; TODO:   ;; (add-hook 'bibtex-mode-hook (lambda () (set-fill-column 120))) ;; TODO to be considered/investigated

;; TODO: check org-roam-bibtex (orb)
;; https://github.com/org-roam/org-roam-bibtex/blob/main/doc/orb-manual.org

;; We use bibio for:
;; - browsing online bibliographic references and importing associated bib entries
;; - fetching the associating pdf file with a reference (in elementaryx-biblio-download-directory)
;; Note that biblio by default biblio-cleanup-bibtex-function is used and internally calls bibtex-clean-entry, hence integrating well with bibtex.el!
(use-package biblio
  :commands biblio-lookup
  :bind  ("M-s b" . biblio-lookup)
  :custom
  (biblio-download-directory elementaryx-biblio-download-directory))
;; https://www.gnu.org/software/emacs/manual/html_node/url/Proxies.html
;; https://intranet.inria.fr/Actualite/Nouveau-proxy-pour-l-acces-aux-abonnements-scientifiques-Inria
;; https://ist-proxy.inria.fr/pac/proxy.pac
;; https://doc-si.inria.fr/display/SU/Proxy+abonnement+inria

;; TODO: a possible better integration of biblio and citar is dicussed here:
;; https://lucidmanager.org/productivity/emacs-bibtex-mode/ (Section "Integrating Biblio and Citar")


;;;;;;;;;;;;;;;;;;;;
;; Spell-checking ;;
;;;;;;;;;;;;;;;;;;;;
;; https://github.com/minad/jinx
;; https://github.com/minad/jinx/wiki
(use-package jinx
  :config
  ;; hook for turning it on globally
  ;; :hook (emacs-startup . global-jinx-mode)
  ;; hooks for turning it on on a per-mode basis
  :hook ((LaTeX-mode . jinx-mode)
	 (latex-mode . jinx-mode)
	 (text-mode . jinx-mode)
	 (org-mode . jinx-mode)
	 (markdown-mode . jinx-mode))
  :bind (("M-$" . jinx-correct)
         ("C-M-$" . jinx-languages)
	 (:map toggle-elementaryx-map
	       ("j" . jinx-mode))))

(use-package vertico-multiform
  :after vertico
  :config
  (add-to-list 'vertico-multiform-categories
               '(jinx grid (vertico-grid-annotate . 20)))
  (vertico-multiform-mode 1))

;; TODO: https://github.com/mpedramfar/zotra

(provide 'elementaryx-write)
